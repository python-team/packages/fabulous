fabulous (0.4.0+dfsg1-1) unstable; urgency=medium

  * New upstream release
  * Remove fonts-noto from depends, add fonts-noto-core to recommends
    (Closes: #983247)
  * Update standards version to 4.6.1

 -- Jonathan Carter <jcc@debian.org>  Thu, 02 Sep 2021 08:09:04 +0200

fabulous (0.3.0+dfsg1-8) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Jonathan Carter ]
  * Update debhelper-compat to level 13
  * Add lintian-override for shared-library-lacks-prerequisites
    (this binary has no prerequisites)

 -- Jonathan Carter <jcc@debian.org>  Thu, 22 Oct 2020 12:06:47 +0200

fabulous (0.3.0+dfsg1-7) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Jonathan Carter ]
  * Fix debian/rules for building xtermspeedup.so (Closes: #952242)
  * Update standards version to 4.5.0
  * Update debhelper-compat to level 12
  * Declare Rules-Requires-Root: no
  * Update copyright years

 -- Jonathan Carter <jcc@debian.org>  Fri, 17 Apr 2020 20:25:27 +0200

fabulous (0.3.0+dfsg1-6) unstable; urgency=medium

  * Move examples back to documentation directory

 -- Jonathan Carter <jcc@debian.org>  Tue, 04 Dec 2018 11:50:49 +0200

fabulous (0.3.0+dfsg1-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces

  [ Jonathan Carter ]
  * Add Breaks+Replaces relationship to python-fabulous
    (Closes: #909913)
  * Replace -doc package and deploy rst file in python-fabulous
    (Closes: #910417)

 -- Jonathan Carter <jcc@debian.org>  Mon, 08 Oct 2018 14:13:19 +0200

fabulous (0.3.0+dfsg1-4) unstable; urgency=medium

  * Version bump (no further changes)

 -- Jonathan Carter <jcc@debian.org>  Fri, 28 Sep 2018 09:20:48 +0200

fabulous (0.3.0+dfsg1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/changelog: Remove trailing whitespaces
  * Convert git repository from git-dpm to gbp layout

  [ Jonathan Carter ]
  * Migrate from python2 to python3 package
  * Update standards version to 4.2.1
  * Add font-noto as dependency
  * wrap-and-sort

 -- Ondřej Nový <onovy@debian.org>  Mon, 14 May 2018 08:04:25 +0200

fabulous (0.3.0+dfsg1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Jonathan Carter ]
  * Adopting package (Closes: #889777)
  * Update compate/debhelper to version 11
  * Update standards version to 4.1.4

 -- Jonathan Carter <jcc@debian.org>  Sun, 29 Apr 2018 13:02:21 +0200

fabulous (0.3.0+dfsg1-1) unstable; urgency=low

  * Team upload.
  * New upstream release
  * Fixed VCS URL (https)
  * d/copyright: Use Files-Excluded to remove non-DFSG files
  * Changed project homepage to new one
  * Use local objects.inv for intersphinx mapping (Closes: #830556)
  * Standards-Version is 3.9.8 now (no changes needed)
  * d/rules: Don't remove non-existent SOURCES.txt from egg-info
  * d/control: Removed pre-wheezy versioned dependencies
  * Enabled autopkgtest-pkg-python testsuite
  * Added dh-python and python-sphinxcontrib.programoutput to build depends
  * Added python-pil to (build-)depends (Closes: #805475)
  * Added fonts-noto-hinted to recommends (Closes: #805473)
  * Install AUTHORS file
  * d/copyright
    - Added more copyright holders
    - Added myself for Debian part
    - Upstream relicensed to Apache-2
  * Install examples into docs examples directory

 -- Ondřej Nový <onovy@debian.org>  Sun, 14 Aug 2016 14:11:56 +0200

fabulous (0.1.5+dfsg1-1) unstable; urgency=low

  * Initial release. (Closes: #705947)
  * The licensing of the fonts file is unclear, hence removal from the source
    package.
  * Patch the except: statements to always specify a type.

 -- Simon Chopin <chopin.simon@gmail.com>  Mon, 08 Jul 2013 19:45:29 -0400
